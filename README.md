# JSONNET

This example usage GitLab CI Child-Parent Pipelines
with [JSONNET](https://jsonnet.org/learning/tutorial.html)
to generate a complex `gitlab-ci.yml` files.

JSONNET provides: functions, variables, loops, and conditionals
that can allow to create a fully paramaterised configs.
